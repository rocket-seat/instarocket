import React, { useState, useEffect, useCallback } from 'react'
import { View, FlatList } from 'react-native'

import LazyImage from '../../components/LazyImage'
import { Post, Header, Name, Loading, Avatar, Description } from './styles';

const Feed = () => {

    const [feed, setFeed] = useState([]);
    const [page, setPage] = useState(1);
    const [total, setTotal] = useState(0);
    const [loading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);
    const [viewable, setViewable] = useState([]);

    async function loadPage(pgNumber = page, shouldRefresh = false) {

        if (total && pgNumber > total) return;

        setLoading(true)

        const res = await fetch('http://10.0.3.2:3000/feed?_expand=author&_limit=5&_page=' + pgNumber)

        const data = await res.json()
        const totalItems = res.headers.get('X-Total-Count')

        setTotal(Math.ceil(totalItems / 5))

        setFeed(shouldRefresh ? data : [...feed, ...data])

        setPage(page + 1)
        setLoading(false)
    }

    useEffect(() => {
        loadPage(1, true)
    }, [])

    async function refreshList() {
        setRefreshing(true)

        await loadPage()

        setRefreshing(false)
    }

    function renderItem(item) {
        return (
            <Post>
                <Header>
                    <Avatar source={{ uri: item.author.avatar }} />
                    <Name>{item.author.name}</Name>
                </Header>

                <LazyImage shouldLoad={viewable.includes(item.id)} aspectRadio={item.aspectRatio} source={{ uri: item.image }} smallSource={{ uri: item.small }} />

                <Description>
                    <Name>{item.author.name}</Name>
                    {item.description}
                </Description>
            </Post>
        )
    }

    const handleViewableItemsChanged = useCallback(({ changed }) => {
        setViewable(changed.map(({ item }) => item.id))
    }, [])

    return (
        <View>
            <FlatList data={feed} keyExtractor={i => String(i.id)} renderItem={({ item }) => renderItem(item)} onEndReached={() => loadPage()} onEndReachedThreshold={0.1} ListFooterComponent={loading && <Loading />} onRefresh={refreshList} refreshing={refreshing} onViewableItemsChanged={handleViewableItemsChanged} viewabilityConfig={{ viewAreaCoveragePercentThreshold: 10 }} />
        </View>
    )
}

export default Feed
