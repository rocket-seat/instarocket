import React, { useState, useEffect } from 'react'

import { Animated } from 'react-native'
import { Small, Original } from './styles'

const OriginalAnimated = Animated.createAnimatedComponent(Original)

const LazyImage = ({ smallSource, source, shouldLoad = false, aspectRadio = 1 }) => {

    const opacity = new Animated.Value(0)
    const [loaded, setLodaded] = useState(false)

    useEffect(() => {
        if (shouldLoad) {
            setTimeout(() => {
                setLodaded(true)
            }, 1000)
        }
    }, [shouldLoad])

    function handleAnimate() {
        Animated.timing(opacity, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true
        }).start()
    }

    return (
        <Small blurRadius={2} source={smallSource} aspect={aspectRadio} resizeMode="contain">
            {loaded && <OriginalAnimated style={{ opacity }} source={source} aspect={aspectRadio} resizeMode="contain" onLoadEnd={handleAnimate} />}
        </Small>
    )
}

export default LazyImage
